module.exports = {
    randString: function (length) {
        if (length === undefined) {
            return 'xxxxxxxxxxxxxxxxxxxx'.replace(/x/g, function (c) {
                return ((Math.random() * 16) | 0).toString(16)
            });
        } else {
            var str = "";
            for (var i = 0; i < length; i++) {
                str = str + "x";
            }
            return str.replace(/x/g, function (c) {
                return ((Math.random() * 16) | 0).toString(16)
            });
        }

    },
    arrayShuffle: function () {
        var tmp, rand;
        for (var i = 0; i < this.length; i++) {
            rand = Math.floor(Math.random() * this.length);
            tmp = this[i];
            this[i] = this[rand];
            this[rand] = tmp;
        }
    },
    uuidv4: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },

    getRandomInt: function (max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

}
