var net = require('net');
var fs = require('fs');

var utils = require('./UTILS');


// init
init();
var config;
var server;
var qCards;
var usedQCards;
var aCards;
var aCardSend;
var qCardSend;

var qSplitSend;
var aSplitSend;

var cancelCountDown;

var GameState = {
    CHOOSE_CARDS: 1,
    VOTING: 2,
    END: 3,
    WAIT_TO_START: 4
}

var currentState = 4;

var players = {};
var playersArray = [];
var connections = {};
var canVote = false;
var canStart = true;

function loadConfig() {
    if (!fs.existsSync("./data/config.json")) {
        console.log("Creating config file...");

        fs.writeFileSync("./data/config.json", JSON.stringify({
            "ADMIN_TOKEN": utils.randString(32),
            "port": 8081,
            "address": "127.0.0.1",
            "SERVER_NAME": "CAH - Server",
            "TIME_LIMIT": false,
            "CHOOSE_TIME": 60,
            "PERMA_CZAR": false,
            "ALLOW_BLANK_CARDS": true,
            "BLANK_CARDS": 5,
            "POINTS_TO_WIN": 5,
            "MAX_PLAYERS": 4,
            "LOGOUT_KICK_TIME": 60,
            "WINNER_TIME": 10,
            "USE_PASSWORD": false,
            "PASSWORD": utils.randString(16),
            "SAVE_SELFW_CARDS": false,
            "NEW_GAME_AUTO": true,
            "ENABLE_CHAT": true
        }, null, 2), "utf8");
    }
    config = JSON.parse(fs.readFileSync("./data/config.json", {encoding: "utf8", flag: "r"}));
}
function validateSettings(settingsJson){
    if (config["ACTION"]!==undefined)delete config["ACTION"];
    if (Object.keys(config).length!==Object.keys(settingsJson).length){
        console.error("Invalid new Settings detected!")
        console.info("------------ Old Settings")
        console.info("Config size: "+Object.keys(config).length);
        var i = 0;
        Object.keys(config).forEach(key=>{
            i++;
            console.info(i+". "+key+": "+config[key]);
        })


        console.info("------------ New Settings")
        i = 0;
        console.info("Config size: "+Object.keys(settingsJson).length);
        Object.keys(settingsJson).forEach(key=>{
            i++;
            console.info(i+". "+key+": "+settingsJson[key]);
        })
        return false;
    }
    return true;
}
function startServer() {
    server = net.createServer(socket => {
        socket.key = socket.remoteAddress + ":" + socket.remotePort;
        console.log("New Connection ("+socket.key+")");
        socket.setEncoding('utf8');

        socket.on('data', function (chunk) {
            try {
                //console.log("Recive data: " + chunk.toString());
                var json = JSON.parse(chunk.toString());
                if (json.ACTION !== undefined) {
                    if (json.ACTION === "LOGIN") {
                        var cfg = JSON.parse(fs.readFileSync("./data/config.json", {encoding: "utf8", flag: "r"}));
                        if (json.NAME === undefined) {
                            socket.write(JSON.stringify({ACTION: "LOGIN", DATA: "NO_NAME", TYPE: "ERR"}));
                            return;
                        }
                        if (currentState !== GameState.WAIT_TO_START) {
                            socket.write(JSON.stringify({ACTION: "LOGIN", DATA: "IN_GAME", TYPE: "ERR"}));
                            return;
                        }
                        if ((((json.PASSWORD !== undefined && cfg.USE_PASSWORD && json.PASSWORD === cfg.PASSWORD) || (json.PASSWORD===cfg.ADMIN_TOKEN)) || !cfg.USE_PASSWORD) && players[json.NAME] === undefined) {
                            var isadm = false;
                            if (json.PASSWORD === cfg.ADMIN_TOKEN) {
                                isadm = true;
                            }
                            var tid = genTmpID();
                            players[json.NAME] = {
                                SOCKET: socket.key,
                                isAdmin: isadm,
                                usedPassword: json.PASSWORD,
                                isLoading: true,
                                points: 0,
                                isCzar: false,
                                isConnected: true,
                                tmpID: tid,
                                isReady: false,
                                isWinner: false
                            };
                            playersArray.push(json.NAME);
                            connections[socket.key] = {NAME: json.NAME, SOCKET: socket}

                            console.log("Player '" + json.NAME + "' connected!");
                            socket.write(JSON.stringify({
                                ACTION: "LOGIN",
                                DATA: "PASSWORD_OK",
                                TYPE: "OK",
                                SNAME: config.SERVER_NAME,
                                players: players[json.NAME].isAdmin,
                                tmpID: tid
                            }));
                            setTimeout(updatePlayers, 500);
                        } else if (players[json.NAME] !== undefined) {
                            socket.write(JSON.stringify({ACTION: "LOGIN", DATA: "NAME_IN_USE", TYPE: "ERR"}));
                            socket.destroy();
                        } else if (config.USE_PASSWORD && json.PASSWORD === undefined) {
                            socket.write(JSON.stringify({ACTION: "LOGIN", DATA: "NO_PASSWORD_SET", TYPE: "ERR"}));
                            socket.destroy();
                        } else if (config.USE_PASSWORD && json.PASSWORD !== config.PASSWORD) {
                            socket.write(JSON.stringify({ACTION: "LOGIN", DATA: "PASSWORD_INCORRECT", TYPE: "ERR"}));
                            socket.destroy();
                        } else {
                            socket.write(JSON.stringify({ACTION: "LOGIN", DATA: "SERVER_ERROR", TYPE: "ERR"}));
                            socket.destroy();
                        }
                    } else if (json.ACTION === "GET_CARDS") {
                        if (getName(socket.key).NAME === undefined) return;
                        if (json.CTYPE !== undefined) {
                            if (json.CTYPE === "Q") {
                                if (json.STATE===undefined){
                                    socket.write(JSON.stringify({
                                        CTYPE: "Q",
                                        TYPE: "OK",
                                        ACTION: "GET_CARDS",
                                        STATE: 0,
                                        MAX: qCardSend.length,
                                        CARDS: qCardSend[0]
                                    }));
                                }else{
                                    socket.write(JSON.stringify({
                                        CTYPE: "Q",
                                        TYPE: "OK",
                                        ACTION: "GET_CARDS",
                                        STATE: json.STATE,
                                        MAX: qCardSend.length,
                                        CARDS: qCardSend[json.STATE]
                                    }));
                                }
                            } else {
                                if (json.STATE===undefined){
                                    socket.write(JSON.stringify({
                                        CTYPE: "A",
                                        TYPE: "OK",
                                        ACTION: "GET_CARDS",
                                        STATE: 0,
                                        MAX: aCardSend.length,
                                        CARDS: aCardSend[0]
                                    }));
                                }else{
                                    socket.write(JSON.stringify({
                                        CTYPE: "A",
                                        TYPE: "OK",
                                        ACTION: "GET_CARDS",
                                        STATE: json.STATE,
                                        MAX: aCardSend.length,
                                        CARDS: aCardSend[json.STATE]
                                    }));
                                }
                            }
                        } else {
                            socket.write(JSON.stringify({
                                CTYPE: "A",
                                TYPE: "OK",
                                ACTION: "GET_CARDS",
                                CARDS: aCards
                            }));
                        }
                    } else if (json.ACTION === "GET_PLAYERS") {
                        if (getName(socket.key).NAME === undefined) return;
                        updatePlayers(socket)
                    } else if (json.ACTION === "START_GAME") {
                        var name = getName(socket.key).NAME;
                        if (name === undefined) return;
                        if (!players[name].isAdmin) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Du hast dafür keine Rechte!"}));
                            return;
                        }
                        if (!canStart){
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Das Spiel kann nicht gestartet werden!"}));
                            return;
                        }
                        startGame();
                    } else if (json.ACTION === "UPDATE_PLAYER_STATE") {
                        var name = getName(socket.key).NAME;
                        if (name === undefined) return;
                        players[name].isLoading = false;
                        updatePlayers();
                    } else if (json.ACTION === "RECONNECT") {
                        if (json.tmpID === undefined) {
                            socket.write(JSON.stringify({ACTION: "RECONNECT", DATA: "FAILED"}));
                            return;
                        }
                        var n = getNameByID(json.tmpID);
                        if (n === null) {
                            socket.write(JSON.stringify({ACTION: "RECONNECT", DATA: "FAILED"}));
                            return;
                        }
                        connections[socket.key] = {SOCKET: socket, NAME: n};
                        players[n].SOCKET = socket.key;
                        players[n].isLoading = true;
                        players[n].isConnected = true;
                        socket.write(JSON.stringify({ACTION: "RECONNECT", DATA: "OK", STATE: currentState, PLAYERS: players}));
                        sendChat("Die Frage wurde übersprungen")
                        setTimeout(updatePlayers, 250);
                    } else if (json.ACTION === "PLAY_CARD") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        playCard(n.NAME, json.ID, json.SLOT, json.SW, json.DATA);
                    } else if (json.ACTION === "SHOW_CARD") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        sendToClients(JSON.stringify(json));
                    } else if (json.ACTION === "VOTE") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        if (canVote) voteCard(json.DATA);
                    } else if (json.ACTION === "GET_SETTINGS") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        var tmp = config;
                        tmp["ACTION"] = json.ACTION;
                        tmp["INIT"] = json.INIT;
                        if (!players[n.NAME].isAdmin){
                            delete tmp["ADMIN_TOKEN"];
                            delete tmp["USE_PASSWORD"];
                            delete tmp["PASSWORD"];
                        }
                        socket.write(JSON.stringify(tmp));
                    } else if (json.ACTION === "STOP_GAME") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        if (!players[n.NAME].isAdmin) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Du hast dafür keine Rechte!"}));
                            return;
                        }
                        if (canStart){
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Um das Spiel zu stoppen musst das Spiel gestartet sein!"}));
                            return;
                        }
                        sendChat("Das Spiel wurde angehalten")
                        stopGame();
                    }else if (json.ACTION === "SET_SETTINGS"){
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        if (!players[n.NAME].isAdmin) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Du hast dafür keine Rechte!"}));
                            return;
                        }
                        if (GameState.WAIT_TO_START!==currentState){
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Die Einstellungen können gerade nicht gespeichert werden!"}));
                            return;
                        }
                        const newSettings = json.DATA;
                        newSettings["INIT"] = true;
                        if (!validateSettings(newSettings)){
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Die Einstellungen sind fehlerhaft!"}));
                            return;
                        }
                        fs.writeFile("./data/config.json", JSON.stringify(newSettings, null, 2), "utf8");
                        socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Die Einstellungen wurden gespeichert! Bitte reloade den Server"}));
                    }else if (json.ACTION === "DUMP_QCARD") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        if (!players[n.NAME].isAdmin) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Du hast dafür keine Rechte!"}));
                            return;
                        }
                        if (GameState.CHOOSE_CARDS!==currentState){
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Die Q - Karte kann nicht verwerfen werden!"}));
                            return;
                        }
                        sendChat("Die Frage wurde übersprungen")
                        dumpQCard();
                    } else if (json.ACTION === "DISCONNECT") {

                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        console.log("Player '"+n.NAME+"' disconnected!")
                        sendChat(name+" hat das Spiel verlassen")
                        removePlayer(n.NAME);
                    } else if (json.ACTION === "KICK_PLAYER") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            return;
                        }
                        if (!players[n.NAME].isAdmin) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Du hast dafür keine Rechte!"}));
                            return;
                        }
                        var name = json.DATA;
                        if (name===undefined||name===null){
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Bitte gib einen Spieler an!"}));
                            return;
                        }
                        var con = connections[players[name].SOCKET];
                        removePlayer(name);
                        sendChat(name+" wurde aus dem Spiel geworfen")
                        if (con!==undefined){
                            var tsocket = con.SOCKET;
                            tsocket.destroy();
                        }
                        updatePlayers();

                    } else if (json.ACTION === "CHAT") {
                        if (config.ENABLE_CHAT){
                            sendToClients(chunk.toString());
                        }else{
                            socket.write(JSON.stringify({ACTION: "CHAT", SMALL: true, TEXT: "Der Chat ist momentan Deaktiviert!"}))
                        }

                    } else if (json.ACTION === "RELOAD") {
                        var n = getName(socket.key);
                        if (n === undefined) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Fehler beim bearbetiten der Anfrage!"}));
                            return;
                        }
                        if (!players[n.NAME].isAdmin) {
                            socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Du hast dafür keine Rechte!"}));
                            return;
                        }
                        reloadServer();

                    }
                }else{
                    socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Fehler beim bearbetiten der Anfrage!"}));
                }
            } catch (e) {
                console.error(e);
                socket.write(JSON.stringify({ACTION: "ALERT", DATA: "Fehler beim bearbetiten der Anfrage!"}));
            }
        });
        socket.on('end', function () {
            var name = getName(socket.key);
            if (name !== undefined && name.NAME !== undefined) {
                players[name.NAME].SOCKET = null;
                players[name.NAME].isConnected = false;
                delete connections[socket.key];
                console.log("Player '" + name.NAME + "' lost connection...");
                startKickCDown(name.NAME, 0);
            } else {
                delete connections[socket.key];
                console.log("Unknown Player lost connection...");
            }
            socket.end();
            socket.destroy();
            updatePlayers();
        });
        socket.on('error', function (err) {
            //connection lost
            console.error(err.code);
            var name = getName(socket.key);
            if (name !== undefined && name.NAME !== undefined) {
                players[name.NAME].SOCKET = null;
                players[name.NAME].isConnected = false;
                delete connections[socket.key];
                updatePlayer(name.NAME);
                console.log("Player '" + name.NAME + "' lost connection...");
                startKickCDown(name.NAME, 0);
            } else {
                delete connections[socket.key];
                console.log("Unknown Player lost connection...");
            }
            socket.end();
            socket.destroy();
            updatePlayers();
        });
    });
    server.maxConnections = config.MAX_PLAYERS + 2;
    server.listen(config.port, config.address);
    console.log('');
    console.log('');
    console.log('---------server details -----------------');
    console.log('Server is listening at port ' + config.port);
    console.log('Server ip: ' + config.address);
    if (config.USE_PASSWORD) console.log('Server password: ' + config.PASSWORD);
    console.log('Admin password: ' + config.ADMIN_TOKEN);
}

function init() {
    cancelCountDown = false;
    qSplitSend = 5;
    aSplitSend = 5;
    usedQCards = [];
    aCardSend = [];
    console.log("Starting Cards against Humanity Server...");
    if (!fs.existsSync("./data/")) fs.mkdirSync("./data/");
    loadConfig();
    if (!fs.existsSync("./data/cards/")) fs.mkdirSync("./data/cards/");
    if (!fs.existsSync("./data/cards/Q/")) fs.mkdirSync("./data/cards/Q/");
    if (!fs.existsSync("./data/cards/A/")) fs.mkdirSync("./data/cards/A/");

    loadCards();

    startServer();


}

function reloadServer(){

    //kick players
    console.log("Reloading server...")
    console.log("Kick all players...")
    Object.entries(players).forEach(([name, s]) => {
        var con = connections[players[name].SOCKET];
        removePlayer(name);
        if (con!==undefined){
            var tsocket = con.SOCKET;
            tsocket.destroy();
        }
    });
    console.log("Kick all players[OK]")


    console.log("Closing server...")
    server.close();
    console.log("Closing server[OK]")
    connections = {};
    players = {};
    playersArray = [];
    console.log("Resetting game...")
    stopGame();
    setTimeout(()=>{
        console.log("Resetting game[OK]")
        console.log("Loading config...")
        loadConfig();
        loadCards();
        console.log("Loading config[OK]")
        setTimeout(()=>{
            console.log("Starting Server...")
            startServer();
            console.log("Starting Server[OK]")
            console.log("Reloading server[OK]")
        },2000);
    },1000*10)

}
function sendChat(msg){
    sendToClients(JSON.stringify({ACTION: "CHAT", SMALL: true, TEXT: msg}));
}

function stopGame() {
    if (currentState!==GameState.WAIT_TO_START&&currentState!==GameState.END){
        cancelCountDown = true;
        resetPlayers();
        setTimeout(()=>{
            cancelCountDown = false;
            setTimeout(()=>{
                setState(GameState.END);
                setTimeout(()=>{
                    setState(GameState.WAIT_TO_START);
                    setTimeout(()=>{
                        updatePlayers();
                        canStart = true;
                    },250);
                },250);
            },250);
        },2000);
    }
}




function loadCards() {
    aCards = [];
    aCardSend = [];
    qCardSend = [];
    canStart = false;
    var tmpBool = true;
    console.log('');
    console.log('');
    console.log('Loading Cards...');
    var files = fs.readdirSync("./data/cards/Q/");
    qCards = [];
    var tmpArray = [];
    var i = 0;
    files.forEach(file => {
        var tmp = JSON.parse(fs.readFileSync("./data/cards/Q/" + file, {encoding: "utf8", flag: "r"}));
        tmp.ID = genCardId();
        tmp.FILE = file;
        qCards.push(tmp);
        tmpArray.push(tmp);
        i++;
        if (i===qSplitSend){
            qCardSend.push(tmpArray);
            tmpArray = [];
            i=0;
        }
    });

    if (tmpArray.length!==0){
        qCardSend.push(tmpArray);
    }


    if (files.length === 0) {
        console.log("No Q cards Found!");
        console.log("Cannot start a Game!");
        tmpBool = false;
    } else if (files.length < config.MAX_PLAYERS * config.POINTS_TO_WIN) {
        console.log("To less 'Q' cards Found!");
        console.log("minimum is " + config.MAX_PLAYERS * config.POINTS_TO_WIN + " at a Maximum of " + config.MAX_PLAYERS + " players! (found " + files.length + " card(s))");
        console.log("Cannot start a Game!");
        tmpBool = false;
    }
    arrayShuffle(qCards);
    files = fs.readdirSync("./data/cards/A/");
    tmpArray = [];
    i = 0;
    files.forEach(file => {
        var tmp = JSON.parse(fs.readFileSync("./data/cards/A/" + file, {encoding: "utf8", flag: "r"}));
        tmp.ID = genCardId();
        tmp.FILE = file;
        tmpArray.push(tmp);
        aCards.push(tmp);
        i++;
        if (i===aSplitSend){
            aCardSend.push(tmpArray);
            tmpArray = [];
            i=0;
        }
    });
    if (tmpArray.length!==0){
        aCardSend.push(tmpArray);
    }
    if (files.length === 0) {
        console.log("No A cards Found!");
        console.log("Cannot start a Game!");
        tmpBool = false;
    } else if (files.length < config.MAX_PLAYERS * config.POINTS_TO_WIN) {
        console.log("To less 'A' cards Found!");
        console.log("minimum is " + config.MAX_PLAYERS * config.POINTS_TO_WIN + " at a Maximum of " + config.MAX_PLAYERS + " players! (found " + files.length + " card(s))");
        console.log("Cannot start a Game!");
        tmpBool = false;
    }
    console.log('');
    console.log('');
    console.log('--------- Loading Cards [OK] -----------------');
    console.log(' » Found '+qCards.length+" qCards");
    console.log(' » Found '+aCards.length+" aCards");
    console.log(' » Can start a game: '+tmpBool);
    canStart = tmpBool;
}



function voteCard(name, timer) {

    if (players[name]===undefined){
        var czar = getCzar();
        if (czar===null) return;
        connections[players[czar].SOCKET].SOCKET.write(JSON.stringify({ACTION: "ALERT", DATA: "Für diesen Spieler kannst du nicht Voten!"}));
        return;
    }
    canVote = false;
    if (timer===undefined){
        timer=10;
        setTimer(timer);
        playedCards={};
        players[name].points = players[name].points+1;
        setTimeout(updatePlayers,250);
        setTimeout(()=>{
            if (!cancelCountDown)voteCard(name,timer-1);
        },1000);
        return;
    }
    setTimer(timer);
    if (timer<=0){
        setTimeout(()=>nextRound(),250);
    }else{
        setTimeout(()=>{
            if (!cancelCountDown)voteCard(name,timer-1);
        },1000);
    }

}


function startKickCDown(name, time) {
    if (time >= config.LOGOUT_KICK_TIME + 1) {
        if (players[name]===undefined)return;
        if (players[name].isConnected) {
            return;
        }
        removePlayer(name)
        updatePlayers();
    } else {
        setTimeout(() => {
            if (players[name]===undefined)return;
            if (players[name].isConnected) {
                return;
            }
            startKickCDown(name, time + 1);
        }, 1000);
    }
}

function removePlayer(name){
    if (players[name]===undefined) {
        return;
    }
        if (connections[players[name].SOCKET] !== undefined && !connections[players[name].SOCKET].ended) connections[players[name].SOCKET].SOCKET.end();
        if (connections[players[name].SOCKET] !== undefined && !connections[players[name].SOCKET].destroyed) connections[players[name].SOCKET].SOCKET.destroy();
    delete connections[players[name].SOCKET];
    delete players[name];
    playersArray.remove(name);
}


function sendToClients(data) {
    Object.entries(players).forEach(([name, s]) => {
        if (players[name].SOCKET !== undefined && players[name].SOCKET !== null) {
            var socket = getName(players[name].SOCKET).SOCKET;
            if (s !== undefined&&players[name].isConnected) {
                socket.write(data);
            }
        }
    });
}

function getData(name) {
    return players[name];
}

function getName(socketKey) {
    return connections[socketKey];
}

function getNameByID(id) {
    var res = null;
    Object.entries(players).forEach(([name, s]) => {
        if (players[name].tmpID === id) {
            res = name;
        }
    });
    return res;
}
function resetPlayers() {
    Object.entries(players).forEach(([name, s]) => {
        players[name].points = 0;
        players[name].isCzar = false;
        players[name].isReady = false;
        players[name].isWinner = false;
    });
}

function updatePlayer(name) {
    if (name === null) return;
    var tmp = players[name];
    tmp["ACTION"] = "UPDATE_PLAYER";
    tmp["NAME"] = name;
    delete tmp.usedPassword;
    sendToClients(JSON.stringify(tmp));
}

function genTmpID() {
    var r = utils.randString(32);
    while (getNameByID(r) !== null) {
        r = utils.randString(32);
    }
    return r;
}


function updatePlayers(socket) {
    var temp = JSON.parse(JSON.stringify(players));
    Object.entries(temp).forEach(([name, data]) => {
        delete temp[name].SOCKET;
        delete temp[name].usedPassword;
    });
    temp['ACTION'] = "UPDATE_PLAYERS";
    if (socket !== undefined) {
        socket.write(JSON.stringify(temp));
        return;
    }
    sendToClients(JSON.stringify(temp));
}


function arrayShuffle(array) {
    var tmp, rand;
    for (var i = 0; i < array.length; i++) {
        rand = Math.floor(Math.random() * array.length);
        tmp = array[i];
        array[i] = array[rand];
        array[rand] = tmp;
    }
}

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

var currentCzarIndex = 0;
var currentQCard;

function startGame() {
    currentCzarIndex = 0;
    canStart = false;


    resetQCards();
    if (Object.keys(players).length < 2) {
        setState(GameState.WAIT_TO_START);
        return;
    }
    var cs = true;
    Object.entries(players).forEach(([name, data]) => {
        if (players[name].isLoading) cs = false;
        if (!players[name].isConnected) cs = false;
    });
    if (!cs){
        sendToClients(JSON.stringify({ACTION: "ALERT", DATA: "Das Spiel kann nicht gestartet werden! (Es sind nicht alle Spieler bereit)"}));
        setState(GameState.WAIT_TO_START);
        return;
    }


    resetPlayers();

    //setting first czar
    while (getCzar() !== null) {
        players[getCzar()].isCzar = false;
    }

    players[playersArray[0]].isCzar = true;


    currentQCard = qCards[utils.getRandomInt(qCards.length)];
    qCards.remove(currentQCard);
    usedQCards.push(currentQCard);
    console.log("Current qCard: "+currentQCard);
    
    updatePlayers();
    setTimeout(()=>{
        setState(GameState.CHOOSE_CARDS)
    },250);
}

function resetQCards() {
    if (usedQCards.length>0){
        usedQCards.forEach(card=>{
            qCards.push(card);
        });
        usedQCards = [];
    }
}

function dumpQCard() {
    if (qCards.length===0){
        resetQCards();
    }
    if (qCards.length===1){
        currentQCard = qCards[0];
    }else{
        currentQCard = qCards[utils.getRandomInt(qCards.length)];
    }

    qCards.remove(currentQCard);
    usedQCards.push(currentQCard);

    updatePlayers();
    setTimeout(()=>setState(GameState.CHOOSE_CARDS),500);
}

var playedCards = {};

function playCard(name, id, slot, sw, data) {
    if (playedCards[name]===undefined){
        playedCards[name] = {};
    }
    if (sw === undefined) {
        sw = false;
    }
    if (sw){
        playedCards[name]["SLOT_"+slot] = {SW: true, TEXT: data};
    }else{
        playedCards[name]["SLOT_"+slot] = id;
    }

    var allReady = true;

    if (Object.keys(playedCards[name]).length===currentQCard.ANSWER_COUNT){
        if (!players[name].isReady){
            players[name].isReady = true;
            updatePlayers();
        }
    }

    if (Object.keys(playedCards).length===Object.keys(players).length-1){
        Object.entries(playedCards).forEach(([name, data]) => {
            if (!players[name].isCzar){
                if (Object.keys(playedCards[name]).length!==currentQCard.ANSWER_COUNT){
                    allReady = false;
                }
            }
        });
    }else{
        allReady = false;
    }

    if (allReady){
        setTimeout(()=>{
            setState(GameState.VOTING)
        },250);
    }
}

function getPlayerMostPoints() {
    var tmp = undefined;
    Object.entries(players).forEach(([name, data]) => {
        if (tmp===undefined){
            tmp = {NAME: name, POINTS:players[name].points};
        }else {
            if (tmp.POINTS<players[name].points) {
                tmp = {NAME: name, POINTS:players[name].points};
            }
        }
    });

    return tmp;
}

function newRound(timer) {
    setTimer(timer);
    if (timer===0){
        startGame();
    }else{
        setTimeout(()=>{
           if (!cancelCountDown) newRound(timer-1);
        },1000);
    }
}


function nextRound() {
    if (Object.keys(players).length < 2) {
        setState(GameState.WAIT_TO_START);
        return;
    }
    var tmp = getPlayerMostPoints();
    if (tmp!==undefined&&tmp.POINTS>=config.POINTS_TO_WIN){
        setTimeout(()=>{
            players[tmp.NAME].isWinner = true;
            setState(GameState.END,tmp.NAME);
            setTimeout(()=>{
                updatePlayers();
                if (config.NEW_GAME_AUTO){
                    setTimeout(()=>{
                        if (!cancelCountDown)newRound(config.WINNER_TIME)
                    },250);
                }else{
                    canStart = true;
                }
            },250);
        },250);
        return;
    }

    Object.entries(players).forEach(([name, s]) => {
        players[name].isCzar = false;
        players[name].isReady = false;
        players[name].isWinner = false;
    });
    ++currentCzarIndex;
    if (playersArray.length === currentCzarIndex) {
        currentCzarIndex = 0;
    }
    if (playersArray[currentCzarIndex] === undefined) {
        currentCzarIndex = 0;
    }
    players[playersArray[currentCzarIndex]].isCzar = true;

    currentQCard = qCards[utils.getRandomInt(qCards.length)];
    qCards.remove(currentQCard);
    usedQCards.push(currentQCard);

    updatePlayers();
    setTimeout(()=>setState(GameState.CHOOSE_CARDS),500);
}


function getCzar() {
    var res = null;
    Object.entries(players).forEach(([name, data]) => {
        if (players[name].isCzar) res = name;
        return res;
    });
    return res;
}

function getNameByIndex(index) {
    var i = 0;
    var res = null;
    Object.entries(players).forEach(([name, data]) => {
        if (i === index) {
            res = name;
            return res;
        }
    });
    return res;
}

function setTimer(value) {
    sendToClients(JSON.stringify({ACTION: "SET_TIMER", DATA: value}));
}

function startVoteTimer(time, ctime) {
    setTimer(ctime);
    if (time >= ctime) {
        setState(2);
    } else {
        setTimeout(() => {
            if (!cancelCountDown)startVoteTimer(time, ctime + 1);
        }, 1000);
    }

}

function setState(state,data) {
    if (state === 1) {
        currentState = state;
        //coose cards
        sendToClients(JSON.stringify({ACTION: "SET_STATE", STATE: state, QCARD: currentQCard.ID}));
        if (config.TIME_LIMIT) {
            startVoteTimer(config.CHOOSE_TIME, 0);
        }
    } else if (state === 2) {
        //voting
        currentState = state;
        canVote = true;
        data = {};

        Object.entries(playedCards).forEach(([name, d]) => {
            data[name] = {};
            Object.entries(playedCards[name]).forEach(([slot, value]) => {
                if (playedCards[name][slot].SW!==undefined&&playedCards[name][slot].SW){
                    data[name][slot] = {CTAG:"SW", TEXT:playedCards[name][slot].TEXT,LICENCE:undefined, WEBSITE:undefined,ID:genCardId(aCards),isShown:false};
                }else{
                    var tmp = getACard(playedCards[name][slot]);
                    tmp.isShown = false;
                    data[name][slot]=tmp;
                }
            });
        });

        sendToClients(JSON.stringify({ACTION: "SET_STATE", STATE: state, DATA:data}));
    } else if (state === 3) {
        //end
        currentState = state;
        if (data!==undefined)players[data].isWinner = true;
        sendToClients(JSON.stringify({ACTION: "SET_STATE", STATE: state, DATA: data}));
    } else if (state === 4) {
        //WAIT_TO_START
        currentState = state;
        sendToClients(JSON.stringify({ACTION: "SET_STATE", STATE: state}));
    }
}


function getQCard(id) {
    var res;
    qCards.forEach(e => {
        if (e.ID === id) {
            res = e;
            return res;
        }
    });
    return res;
}
function getACard(id) {
    var res;
    aCards.forEach(e => {
        if (e.ID === id) {
            res = e;
            return res;
        }
    });
    return res;
}


function genCardId(arrayList) {
    var id = utils.randString(16);

    while (cardIdExists(arrayList,id)){
        id = utils.randString(16);
    }
    return id;
}

function cardIdExists(arrayList, id) {
    var exists = false;
    if (arrayList===undefined) return false;
    arrayList.forEach(e=>{
        if (e.ID!==undefined&&e.ID===id) {
            exists = true;
            return exists;
        }
    });
    return exists;
}

